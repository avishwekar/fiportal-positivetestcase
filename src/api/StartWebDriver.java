package api;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;


public class StartWebDriver {
	
	public static WebDriver driver = null;
    public static final Logger logger = LogManager.getLogger();

	

	@BeforeSuite
	public void setUp() {
		try {
			ReadConfigProperty file = new ReadConfigProperty();
			if("firefox".equalsIgnoreCase(file.getBrowser())) {
				driver = new FirefoxDriver();
			} else if("chrome".equalsIgnoreCase(file.getBrowser())) {
				System.setProperty("webdriver.chrome.driver", StartWebDriver.class.getClassLoader().getResource("resource/chromedriver").getPath());
			    driver = new ChromeDriver();
			} else if("explorer".equalsIgnoreCase(file.getBrowser())) {
				System.setProperty("webdriver.ie.driver", StartWebDriver.class.getClassLoader().getResource("resource/IEDriverServer.exe").getPath());
			    driver = new InternetExplorerDriver();
			}else {
				driver = new HtmlUnitDriver();
			}
			driver.get(file.getUrl());
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	    

		
	}
	
	public static void waitTillPageExists() {
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	public static WebElement getElement(String locator) {
		boolean flag = false;
		if(locator.contains("/"))
			flag = true;
		if(driver.findElements(By.id(locator)).size() ==  1) {
			return driver.findElement(By.id(locator));
		} else if(driver.findElements(By.xpath(locator)).size() == 1) {
			return driver.findElement(By.xpath(locator)); 
		} else if(driver.findElements(By.xpath(locator)).size() == 1) {
			return driver.findElement(By.xpath(locator));
		} else if(driver.findElements(By.cssSelector(locator)).size() == 1) {
			return driver.findElement(By.cssSelector(locator));
		} else if(driver.findElements(By.className(locator)).size() == 1) {
			return driver.findElement(By.className(locator));
		} else if(driver.findElements(By.name(locator)).size() == 1) {
			return driver.findElement(By.name(locator));
		} 
		else
			throw new NoSuchElementException("No Such Element"+locator);
	}

	  /* Will wait until a certain element exists inside a WebDriver, searches using By selectors
	     * @param driver    the current WebDriver instance
	     * @param selector  the By selector we are searching for
	     * @param timeout   the number of seconds until this request times out
	     * @param sleep     the number of milliseconds between each poll
	     * @return the WebElement we have found
	     */
	    public WebElement waitUntilExists(WebDriver driver, By selector, long timeout, long sleep){
	        try {
	        	Thread.sleep(sleep);
	            return (new WebDriverWait(driver, timeout, sleep)).until(ExpectedConditions.visibilityOfElementLocated(selector));
	        }
	        catch(TimeoutException e){
	            return null;
	        }
	        catch(Exception e){
	            log().error("Unexpected error occurred while waiting for element to exist.");
	            return null;
	        }
	    }
	  
	    public static Logger log() {
	    	return StartWebDriver.logger;
		}

	    


		@AfterSuite(alwaysRun=true)
		public void tearDown() {
			try {
				driver.close();
				driver.quit();
				if(driver != null)
					driver = null;
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
}