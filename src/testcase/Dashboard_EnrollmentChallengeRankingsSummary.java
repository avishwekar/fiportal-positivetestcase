package testcase;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import api.StartWebDriver;
import api.TextBoxHelper;

public class Dashboard_EnrollmentChallengeRankingsSummary extends StartWebDriver {

	@Test
	public void testBuzzPointsUserPage() throws Exception  {

				
		try {
			//SR Level 1 user
			TextBoxHelper.typeInTextBox("usernameSignIn", "srlevel1");
			TextBoxHelper.typeInTextBox("//div[@id='content']/descendant::input[position()=2]", "Aaaaaaa1");
			driver.findElement(By.xpath("//button[@type='submit']")).click();
			AssertJUnit.assertTrue(driver.getTitle().contains("Sign In")); 
			Thread.sleep(6000);
			
			//SR Level2 user
			driver.get("http://hetzner.buzzpoints.com:3010/");
			TextBoxHelper.typeInTextBox("usernameSignIn", "srlevel2");
			TextBoxHelper.typeInTextBox("//div[@id='content']/descendant::input[position()=2]", "Aaaaaaa1");
			driver.findElement(By.xpath("//button[@type='submit']")).click();
			AssertJUnit.assertTrue(driver.getTitle().contains("Sign In")); 
			Thread.sleep(6000);
			
			//FI Magaer
			driver.get("http://hetzner.buzzpoints.com:3010/");
			TextBoxHelper.typeInTextBox("usernameSignIn", "fimgrcadence");
			TextBoxHelper.typeInTextBox("//div[@id='content']/descendant::input[position()=2]", "Aaaaaaa1");
			driver.findElement(By.xpath("//button[@type='submit']")).click();
			AssertJUnit.assertTrue(driver.getTitle().contains("Sign In")); 
			Thread.sleep(6000);
		    
		} catch(NoSuchElementException e){
			e.printStackTrace();
		}
	}
}