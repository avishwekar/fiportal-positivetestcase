package testcase;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import api.GenericHelper;
import api.LinkHelper;
import api.StartWebDriver;
import api.TextBoxHelper;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Dashboard_Main extends StartWebDriver {
	
	@Test
	public void testDashboard() throws Exception {
		try {
		TextBoxHelper.typeInTextBox("usernameSignIn", "admincadence");
		TextBoxHelper.typeInTextBox("//div[@id='content']/descendant::input[position()=2]", "Aaaaaaa1");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		AssertJUnit.assertTrue(driver.getTitle().contains("Sign In")); 
	    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		
	    driver.findElement(By.xpath("//li[@id='nav-reports-link']/a/span[2]")).click();
	    LinkHelper.clickLink("Reward Redemption Report");
	    LinkHelper.clickLink("Preferred Local Business Report");
	    LinkHelper.clickLink("Enrollment Report");
	    driver.findElement(By.xpath("//li[@id='nav-dashboard-link']/a/span[2]")).click();
	    LinkHelper.clickLink("Buzz Points Users");
	    driver.findElement(By.xpath("//li[@id='nav-reports-link']/a/span[2]")).click();
	    LinkHelper.clickLink("Files");
	    LinkHelper.clickLink("Administration");
	    LinkHelper.clickLink("Marketing");
	    driver.findElement(By.xpath("//li[@id='nav-support-link']/a/span[2]")).click();
	    driver.findElement(By.xpath("//div[@id='bs-example-navbar-collapse-1']/ul/li[5]/a/div/span[2]")).click();
	    System.out.println("Test Passed"+this.getClass());

		//Take Screenshot
		GenericHelper.takeScreenShot(this.getClass().toString());
		} catch(Exception e) {
			
			//Take Screenshot
			GenericHelper.takeScreenShot(this.getClass().toString());
			e.printStackTrace();
		}

	}
	


}
