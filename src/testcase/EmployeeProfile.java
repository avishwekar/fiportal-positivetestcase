package testcase;


import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import api.AlertHelper;
import api.ComboBoxHelper;
import api.GenericHelper;
import api.StartWebDriver;
import api.TextBoxHelper;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class EmployeeProfile extends StartWebDriver {

	@Test
	  public void testEmployeeProfile() throws Exception {
		
		try {
		TextBoxHelper.typeInTextBox("usernameSignIn", "admincadence");
		TextBoxHelper.typeInTextBox("//div[@id='content']/descendant::input[position()=2]", "Aaaaaaa1");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		AssertJUnit.assertTrue(driver.getTitle().contains("Sign In")); 
	    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	    driver.findElement(By.xpath("//li[@id='nav-institution-mgmt-link']/a/span[2]")).click();
	    ComboBoxHelper.selectByVisibleText("(//select[@type='select'])[3]", "Active");
	    driver.findElement(By.id("userSearchBtn")).click();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.pollingEvery(1, TimeUnit.SECONDS);	

	    wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.cssSelector("button.searchDataDetail"))));		
		driver.findElement(By.cssSelector("button.searchDataDetail")).click();
	    GenericHelper.takeScreenShot(this.getClass().toString()+"ActiveUser");
	    
	    wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.cssSelector("button.close"))));		
	    driver.findElement(By.cssSelector("button.close")).click();
	    driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);	  
	    
	    ComboBoxHelper.selectByVisibleText("(//select[@type='select'])[3]", "Pending");
	    driver.findElement(By.id("userSearchBtn")).click();
	    wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.cssSelector("button.searchDataDetail"))));		
	    driver.findElement(By.cssSelector("button.searchDataDetail")).click();
	    GenericHelper.takeScreenShot(this.getClass().toString()+"PendingUser");
	    wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.cssSelector("button.close"))));		
	    driver.findElement(By.cssSelector("button.close")).click();
	    
	    TextBoxHelper.typeInTextBox("//div//input[@id='fi-form-first-name']", "T");
	    driver.findElement(By.id("userSearchBtn")).click();
	    Thread.sleep(6000);
	    
	    TextBoxHelper.typeInTextBox("fi-form-last-name", "d");
	    driver.findElement(By.id("userSearchBtn")).click();
	    Thread.sleep(6000);
	    
	    System.out.println("Test Passed"+this.getClass());

		} catch(Exception e) {
			
			//Take Screenshot
			GenericHelper.takeScreenShot(this.getClass().toString());
			e.printStackTrace();
		}
	   
	  }
}
