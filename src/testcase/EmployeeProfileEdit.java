package testcase;


import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import api.ComboBoxHelper;
import api.GenericHelper;
import api.StartWebDriver;
import api.TextBoxHelper;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

public class EmployeeProfileEdit extends StartWebDriver {

	@Test
	  public void testEmployeeProfileEdit() throws Exception {
		
		try {
		//Buzz Admin user
		TextBoxHelper.typeInTextBox("usernameSignIn", "admincadence");
		TextBoxHelper.typeInTextBox("//div[@id='content']/descendant::input[position()=2]", "Aaaaaaa1");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		AssertJUnit.assertTrue(driver.getTitle().contains("Sign In")); 
		Thread.sleep(6000);
		
	    driver.findElement(By.xpath("//li[@id='nav-institution-mgmt-link']/a/span[2]")).click();
	    Thread.sleep(6000);
	    driver.findElement(By.xpath("//table[@id='userSearchResults']/tbody/tr[10]/td[7]/button")).click();
	    Thread.sleep(6000);
	    driver.findElement(By.id("rolePencil")).click();
	    driver.findElement(By.id("branchPencil")).click();
	    ComboBoxHelper.selectByVisibleText("(//select[@id='branchId'])[2]", "ALBERTVILLE BRANCH");
	    driver.findElement(By.id("emailPencil")).click();
	    driver.findElement(By.id("email")).clear();
	    TextBoxHelper.typeInTextBox("email", "tbrown+sr1b@buzzoinptinc.com");
	    driver.findElement(By.cssSelector("button.close")).click();
	    
	    //SR Level2 user
	    driver.get("http://hetzner.buzzpoints.com:3010/");
	    TextBoxHelper.typeInTextBox("usernameSignIn", "srlevel2");
		TextBoxHelper.typeInTextBox("//div[@id='content']/descendant::input[position()=2]", "Aaaaaaa1");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		AssertJUnit.assertTrue(driver.getTitle().contains("Sign In")); 
		Thread.sleep(6000);
		
	    driver.findElement(By.xpath("//li[@id='nav-institution-mgmt-link']/a/span[2]")).click();
	    Thread.sleep(6000);
	    driver.findElement(By.xpath("//table[@id='userSearchResults']/tbody/tr[10]/td[7]/button")).click();
	    Thread.sleep(6000);
	    driver.findElement(By.id("rolePencil")).click();
	    driver.findElement(By.id("branchPencil")).click();
	    ComboBoxHelper.selectByVisibleText("(//select[@id='branchId'])[2]", "ALBERTVILLE BRANCH");
	    driver.findElement(By.id("emailPencil")).click();
	    driver.findElement(By.id("email")).clear();
	    TextBoxHelper.typeInTextBox("email", "tbrown+sr1b@buzzoinptinc.com");
	    driver.findElement(By.cssSelector("button.close")).click();
	    
	    //FI Manager user
	    driver.get("http://hetzner.buzzpoints.com:3010/");
	    TextBoxHelper.typeInTextBox("usernameSignIn", "fimgrcadence");
		TextBoxHelper.typeInTextBox("//div[@id='content']/descendant::input[position()=2]", "Aaaaaaa1");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		AssertJUnit.assertTrue(driver.getTitle().contains("Sign In")); 
		Thread.sleep(6000);
		
	    driver.findElement(By.xpath("//li[@id='nav-institution-mgmt-link']/a/span[2]")).click();
	    Thread.sleep(6000);
	    driver.findElement(By.xpath("//table[@id='userSearchResults']/tbody/tr[10]/td[7]/button")).click();
	    Thread.sleep(6000);
	    driver.findElement(By.id("rolePencil")).click();
	    driver.findElement(By.id("branchPencil")).click();
	    ComboBoxHelper.selectByVisibleText("(//select[@id='branchId'])[2]", "ALBERTVILLE BRANCH");
	    driver.findElement(By.id("emailPencil")).click();
	    driver.findElement(By.id("email")).clear();
	    TextBoxHelper.typeInTextBox("email", "tbrown+sr1b@buzzoinptinc.com");
	    driver.findElement(By.cssSelector("button.close")).click();
	    System.out.println("Test Passed"+this.getClass());

		//Take Screenshot
		GenericHelper.takeScreenShot(this.getClass().toString());
		} catch(Exception e) {
			
			//Take Screenshot
			GenericHelper.takeScreenShot(this.getClass().toString());
			e.printStackTrace();
		}
	}
}
