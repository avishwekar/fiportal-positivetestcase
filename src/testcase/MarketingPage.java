package testcase;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import api.*;

public class MarketingPage extends StartWebDriver {

	@Test
	public void testMarketingPage() throws Exception {

		try {
		    
		    //FI Admin user
		    driver.get("http://hetzner.buzzpoints.com:3010/");
		 	driver.findElement(By.id("usernameSignIn")).clear();
		 	driver.findElement(By.id("usernameSignIn")).sendKeys("admincadence");
		 	driver.findElement(By.xpath("//input[@type='password']")).clear();
		    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaa1");
		    driver.findElement(By.xpath("//button[@type='submit']")).click();Thread.sleep(6000);
		    driver.findElement(By.cssSelector("a[href*='/#/marketing']")).click();

		    
		    //FI Reports Viewer user
		    driver.get("http://hetzner.buzzpoints.com:3010/");
		 	driver.findElement(By.id("usernameSignIn")).clear();
		 	driver.findElement(By.id("usernameSignIn")).sendKeys("analystcadence");
		 	driver.findElement(By.xpath("//input[@type='password']")).clear();
		    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaa1");
		    driver.findElement(By.xpath("//button[@type='submit']")).click();Thread.sleep(6000);
		    driver.findElement(By.cssSelector("a[href*='/#/marketing']")).click();

		    
		    //Buzz Admin user
		    driver.get("http://hetzner.buzzpoints.com:3010/");
		 	driver.findElement(By.id("usernameSignIn")).clear();
		 	driver.findElement(By.id("usernameSignIn")).sendKeys("buzzadmincadence");
		 	driver.findElement(By.xpath("//input[@type='password']")).clear();
		    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaa1");
		    driver.findElement(By.xpath("//button[@type='submit']")).click();Thread.sleep(6000);
		    driver.findElement(By.cssSelector("a[href*='/#/marketing']")).click();

		    System.out.println("Test Passed :"+this.getClass());
	
			//Take Screenshot
			GenericHelper.takeScreenShot(this.getClass().toString());
		} catch(Exception e) {
			
			//Take Screenshot
			GenericHelper.takeScreenShot(this.getClass().toString());
			e.printStackTrace();
		}


		
	}
}
