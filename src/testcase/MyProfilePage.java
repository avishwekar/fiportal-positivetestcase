package testcase;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.openqa.selenium.By;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import api.*;

public class MyProfilePage extends StartWebDriver {

	@Test
	  public void testMyProfilePage() throws Exception {
		
		try {
			
			TextBoxHelper.typeInTextBox("usernameSignIn", "admincadence");
			TextBoxHelper.typeInTextBox("//div[@id='content']/descendant::input[position()=2]", "Aaaaaaa1");
			driver.findElement(By.xpath("//button[@type='submit']")).click();
			AssertJUnit.assertTrue(driver.getTitle().contains("Sign In")); 
			Thread.sleep(6000);
		
			driver.findElement(By.xpath("//span[text()='admincadence']")).click();
		    driver.findElement(By.linkText("My Profile")).click();
		    Thread.sleep(6000);
		    driver.findElement(By.xpath("//div[@id='content']/div/div/div[2]/div/div[3]/div/form/div/div[2]/div[3]/div/label/div/span/span[3]/img")).click();
		    driver.findElement(By.xpath("//div[@id='content']/div/div/div[2]/div/div[3]/div/form/div/div[2]/div[3]/div/label/div/span/span[3]/img")).click();
		    Thread.sleep(6000);
		    driver.findElement(By.id("emailPencil")).click();
		    driver.findElement(By.id("email")).clear();
		    driver.findElement(By.id("email")).sendKeys("jmata+admincabuzzpointsinc.com");
		    Thread.sleep(3000);
		    driver.findElement(By.id("email")).clear();
		    driver.findElement(By.id("email")).sendKeys("jmata+adminca@buzzpointsinc.com");
		    driver.findElement(By.id("submitButton")).click();
		    System.out.println("Test Passed"+this.getClass());

			//Take Screenshot
			GenericHelper.takeScreenShot(this.getClass().toString());
			} catch(Exception e) {
				
				//Take Screenshot
				GenericHelper.takeScreenShot(this.getClass().toString());
				e.printStackTrace();
			    System.out.println("Test Failed"+this.getClass());

			}
	  }
}
