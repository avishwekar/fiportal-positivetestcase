package testcase;

import org.testng.annotations.Test;

import api.GenericHelper;
import api.LinkHelper;
import api.StartWebDriver;
import api.TextBoxHelper;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver.Timeouts;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ReportPage_EnrollmentReport extends StartWebDriver {

	@Test
	public void testEnrollmentReport() throws Exception {

		try {
		TextBoxHelper.typeInTextBox("usernameSignIn", "admincadence");
		TextBoxHelper.typeInTextBox("//div[@id='content']/descendant::input[position()=2]", "Aaaaaaa1");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		Assert.assertTrue(driver.getTitle().contains("Sign In"));
		
	    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	    	    
	    LinkHelper.clickLink("Reports");
	    WebElement iframe = driver.findElement(By.xpath("//iframe"));
	    driver.switchTo().frame(iframe);
	    
	    WebElement exportButton = waitUntilExists(driver, By.cssSelector("span[class='exportButton']"), 10l, 600l);
		if (exportButton == null)
			throw new Exception("Could not found span[class='exportButton']");
	   driver.findElement(By.id("render_dateRange")).click();Thread.sleep(6000);
	   driver.findElement(By.linkText("Year to date")).click();Thread.sleep(6000);
	   driver.findElement(By.id("render_dateRange2")).click();Thread.sleep(6000);
	   driver.findElement(By.xpath("(//a[contains(text(),'Specific Date')])[2]")).click();Thread.sleep(6000);
	   driver.findElement(By.xpath("(//a[contains(text(),'Date Range')])[2]")).click();Thread.sleep(6000);
	   driver.findElement(By.xpath("(//a[contains(text(),'22')])[4]")).click();Thread.sleep(6000);
	   driver.findElement(By.xpath("(//a[contains(text(),'21')])[3]")).click();Thread.sleep(6000);
	   driver.findElement(By.xpath("//div[9]/div/div/button")).click();Thread.sleep(6000);
	   driver.findElement(By.id("render_dateRange")).click();Thread.sleep(6000);
	   driver.findElement(By.xpath("(//a[contains(text(),'Date Range')])[3]")).click();Thread.sleep(6000);
//	   driver.findElement(By.xpath("//div[@id='dp1466609283989']/div/div/a[2]")).click();Thread.sleep(6000);
	   driver.findElement(By.xpath("(//a[contains(text(),'21')])[5]")).click();Thread.sleep(6000);
	   driver.findElement(By.xpath("//div[10]/div/div/button")).click();Thread.sleep(6000);
	   driver.findElement(By.xpath("//div[@id='reportColTable_filter']//input")).clear();
	   driver.findElement(By.xpath("//div[@id='reportColTable_filter']//input")).sendKeys("Buzz");Thread.sleep(6000);
	    driver.findElement(By.cssSelector("span[class='exportButton']")).click();
	    
	    WebElement button = waitUntilExists(driver, By.cssSelector("span[class='exportButto']"), 10l, 600l);
	    driver.switchTo().defaultContent();

	   // Take Screenshot of entire window
	    JavascriptExecutor exe = (JavascriptExecutor)driver;
	    exe.executeScript("window.scrollTo(0,0)");
		Boolean check = (Boolean)exe.executeScript("return document.documentElement.scrollHeight>document.documentElement.clientHeight");
		Long scrollH = (Long)exe.executeScript("document.documentElement.scrollHeight");
		Long clientH = (Long)exe.executeScript("document.documentElement.clientHeight");
		int index = 1;
		if(check.booleanValue()) {
			if(scrollH != null)
				while(scrollH.intValue() != 0) {
				GenericHelper.takeScreenShot("Screen -"+index);
				exe.executeScript("window.scrollTo(0,"+clientH*index+")");
				scrollH = scrollH-clientH;
			    driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				index++;
			}
		}
		else
			GenericHelper.takeScreenShot(this.getClass().toString());
		System.out.println("Test Passed"+this.getClass());

		//Take Screenshot
		GenericHelper.takeScreenShot(this.getClass().toString());
		} catch(Exception e) {
			
			//Take Screenshot
			GenericHelper.takeScreenShot(this.getClass().toString());
			e.printStackTrace();
			System.out.println("Test Failed"+this.getClass());

		}
		
	}
}