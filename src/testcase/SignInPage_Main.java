package testcase;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import api.StartWebDriver;
import api.TextBoxHelper;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

/*
 * Sign in the page using valid username and
 * password
 */

public class SignInPage_Main extends StartWebDriver {
	
	@Test
	public void testsignInPage() throws Exception{
		
		//Enter your username and password and click on the Sign In button  
	    driver.get("http://hetzner.buzzpoints.com:3010/");
	    WebElement ele = driver.findElement(By.xpath("//a"));
	    Assert.assertNotNull(ele);
	    driver.findElement(By.id("usernameSignIn")).clear();
	    driver.findElement(By.id("usernameSignIn")).sendKeys("admincadence");
	    driver.findElement(By.xpath("//input[@type='password']")).clear();
	    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaa1");
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    
	    //	Go to the sign in page, enter an incorrect username and a correct password and click on the Sign In button
	    driver.get("http://hetzner.buzzpoints.com:3010/");
	    driver.findElement(By.id("usernameSignIn")).clear();
	    driver.findElement(By.id("usernameSignIn")).sendKeys("admincadencee");
	    driver.findElement(By.xpath("//input[@type='password']")).clear();
	    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaa1");
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    
	    //Go to the sign in page, enter an incorrect password and a correct username and click on the Sign In button
	    driver.get("http://hetzner.buzzpoints.com:3010/");
	    driver.findElement(By.id("usernameSignIn")).clear();
	    driver.findElement(By.id("usernameSignIn")).sendKeys("admincadence");
	    driver.findElement(By.xpath("//input[@type='password']")).clear();
	    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaaa1");
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    
	    //Enter an incorrect username and incorrect password and click on the Sign In button
	    driver.get("http://hetzner.buzzpoints.com:3010/");
	    driver.findElement(By.id("usernameSignIn")).clear();
	    driver.findElement(By.id("usernameSignIn")).sendKeys("admincaaadence");
	    driver.findElement(By.xpath("//input[@type='password']")).clear();
	    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaaa11");
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	    
	    //Enter credentials for an disabled user and click on the Sign In button
	    driver.get("http://hetzner.buzzpoints.com:3010/");
	    driver.findElement(By.id("usernameSignIn")).clear();
	    driver.findElement(By.id("usernameSignIn")).sendKeys("srdeact2");
	    driver.findElement(By.xpath("//input[@type='password']")).clear();
	    driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Aaaaaaaa11");
	    driver.findElement(By.xpath("//button[@type='submit']")).click();
	}

}